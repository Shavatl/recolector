package com.example.mader;

import android.app.Activity; 
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.os.AsyncTask;



import android.view.View.OnClickListener;
import android.widget.Button;




public class Control extends Activity {

    
	
	Button arriba, abajo,derecha,izquierda,pausa,encender,apagar, detener;
    private String address = null;
    private ProgressDialog progress;
    BluetoothAdapter myBluetooth = null;
    BluetoothSocket btSocket = null;
    private boolean isBtConnected = false;
    private Recolector Servicio_BT = null;
    private BluetoothAdapter AdaptadorBT = null;
    
    static final UUID myUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    

	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_control);
		


        Intent newint = getIntent();
        
        address = newint.getStringExtra(Recolector.EXTRA_ADDRESS); //recivimos la mac address obtenida en la actividad anterior
        
        
        arriba = (Button)findViewById(R.id.adelante);
        abajo = (Button)findViewById(R.id.atras);
        derecha = (Button)findViewById(R.id.derecha);
        izquierda = (Button)findViewById(R.id.izquierda);
        detener = (Button)findViewById(R.id.detener);
        encender = (Button)findViewById(R.id.encender);
        apagar = (Button)findViewById(R.id.apagar);
        pausa = (Button)findViewById(R.id.giro);
        


        new ConnectBT().execute();
        
        arriba.setOnClickListener(new View.OnClickListener() 
     	{
				public void onClick(View vv) {   
						 arri();
						 

					}	                            
				
			});
        
        abajo.setOnClickListener(new View.OnClickListener() 
     	{
				public void onClick(View vv) {   
			
						 
						 aba();
					}	                            
				
			});
        
        derecha.setOnClickListener(new View.OnClickListener() 
     	{
				public void onClick(View vv) {   
			
						 
						 dere();
					}	                            
				
			});
        
        izquierda.setOnClickListener(new View.OnClickListener() 
     	{
				public void onClick(View vv) {   
			
						 
						 izq();
					}	                            
				
			});
        
        detener.setOnClickListener(new View.OnClickListener() 
     	{
				public void onClick(View vv) {   
			
						 
						 dete();
					}	                            
				
			});
        
        
        encender.setOnClickListener(new View.OnClickListener() 
     	{
				public void onClick(View vv) {   
			
						 
						 ence();
					}	                            
				
			});
        
        apagar.setOnClickListener(new View.OnClickListener() 
     	{
				public void onClick(View vv) {   
			
						 
						 apa();
					}	                            
				
			});
        
        pausa.setOnClickListener(new View.OnClickListener() 
     	{
				public void onClick(View vv) {   
			
						 
						 paus();
					}	                            
				
			});
        
        
        
        
}
	
	
	
	
	private void arri()
    {
    	if (btSocket!=null)
        {
            try
            {
                btSocket.getOutputStream().write("a".toString().getBytes());
            }
            catch (IOException e)
            {
              
            }
        }
    }
    
	private void aba()
    {
    	if (btSocket!=null)
        {
            try
            {
                btSocket.getOutputStream().write("b".toString().getBytes());
            }
            catch (IOException e)
            {
              
            }
        }
    }
	
	private void dere()
    {
    	if (btSocket!=null)
        {
            try
            {
                btSocket.getOutputStream().write("d".toString().getBytes());
            }
            catch (IOException e)
            {
              
            }
        }
    }
	
	private void izq()
    {
    	if (btSocket!=null)
        {
            try
            {
                btSocket.getOutputStream().write("i".toString().getBytes());
            }
            catch (IOException e)
            {
              
            }
        }
    }
	
	private void dete()
    {
    	if (btSocket!=null)
        {
            try
            {
                btSocket.getOutputStream().write("s".toString().getBytes());
            }
            catch (IOException e)
            {
              
            }
        }
    }
	
	private void ence()
    {
    	if (btSocket!=null)
        {
            try
            {
                btSocket.getOutputStream().write("o".toString().getBytes());
            }
            catch (IOException e)
            {
              
            }
        }
    }
	
	private void apa()
    {
    	if (btSocket!=null)
        {
            try
            {
                btSocket.getOutputStream().write("f".toString().getBytes());
            }
            catch (IOException e)
            {
              
            }
        }
    }
	
	private void paus()
    {
    	if (btSocket!=null)
        {
            try
            {
                btSocket.getOutputStream().write("u".toString().getBytes());
            }
            catch (IOException e)
            {
              
            }
        }
    }
    
    private boolean ConnectSuccess = true;

    
    private class ConnectBT extends AsyncTask<Void, Void, Void>  // Coneccion del UII
    {
        private boolean ConnectSuccess = true;

        @Override
        protected void onPreExecute()
        {
            progress = ProgressDialog.show(Control.this, "Conectando...", "Espere por favor!!!");
        }

        @Override
        protected Void doInBackground(Void... devices)
        {
            try
            {
                if (btSocket == null || !isBtConnected)
                {
                 myBluetooth = BluetoothAdapter.getDefaultAdapter();
                 BluetoothDevice dispositivo = myBluetooth.getRemoteDevice(address);//coneccion al dispositivo y checa si esta disponible
                 btSocket = dispositivo.createInsecureRfcommSocketToServiceRecord(myUUID);
                 BluetoothAdapter.getDefaultAdapter().cancelDiscovery();
                 btSocket.connect();
                }
            }
            catch (IOException e)
            {
                ConnectSuccess = false;
            }
            return null;
        }
    }

    
          

	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}

