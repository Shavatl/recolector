# Recolector

Proyecto escolar el cual era un carro autónomo el cual se encargaba de recolectar basura el cual también podía ser controlado por medio de una app móvil

# Authors

* Salvador Quintero - (Developer, Designer) [[Gitlab](https://gitlab.com/Shavatl)]

## Others
```
Si tienes dudas o quieres contribuir con el proyecto o mejorarlo te puedes poner en contacto conmigo desde mi correo 
crispinquintero@gmail.co o por este medio, gracias.

```